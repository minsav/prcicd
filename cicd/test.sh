#!/bin/bash
result=$(echo 5 | ./src/fibonacci)
expected=3
echo "res=$result and exp=$expected"

result=$(echo 10 | ./src/fibonacci)
expected=34
echo "res=$result and exp=$expected"

result=$(echo 1 | ./src/fibonacci)
expected=0
echo "res=$result and exp=$expected"

result=$(echo 0 | ./src/fibonacci)
expected=""
echo "res=$result and exp=$expected"

result=$(echo -5 | ./src/fibonacci)
expected=""
echo "res=$result and exp=$expected"

result=$(echo a | /src/fibonacci)
expected=""
echo "res=$result and exp=$expected"
