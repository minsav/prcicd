FROM ubuntu:22.04
COPY *.deb /deb/
RUN apt-get update
RUN apt-get install -y /deb/*.deb
CMD ["/bin/bash"]